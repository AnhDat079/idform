package com.example.form;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

     ListView lvInfo;
     EditText editId, editName, editNote;
     Button btnSave;
     private List<Info> info;
     private InfoAdapter infoAdapter;
     private Info information;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UI_SETUP();

        info = new ArrayList<>();
        infoAdapter = new InfoAdapter(MainActivity.this,info,R.layout.custom_form);

        btnSave.setOnClickListener(this);

    }

    private void UI_SETUP() {
        lvInfo = (ListView) findViewById(R.id.lvInfo);
        editId = (EditText) findViewById(R.id.editId);
        editName = (EditText) findViewById(R.id.editName);
        editNote = (EditText) findViewById(R.id.editNote);
        btnSave = (Button) findViewById(R.id.btnSave);


    }

    public void Delete(final int i){
        Toast.makeText(this, "Do you want delete???", Toast.LENGTH_SHORT).show();
        info.remove(i);
        infoAdapter.notifyDataSetChanged();

    }
    public void Edit(final int i){
        Toast.makeText(this, "Do you want edit???", Toast.LENGTH_SHORT).show();
        final Info infor = info.get(i);
        View dialogSheetView = LayoutInflater.from(MainActivity.this).inflate(R.layout.edit_info,null );
        BottomSheetDialog dialog = new BottomSheetDialog(MainActivity.this);
        dialog.setContentView(dialogSheetView);

        EditText editId = (EditText) dialogSheetView.findViewById(R.id.editId);
        EditText editName = (EditText) dialogSheetView.findViewById(R.id.editName);
        EditText editNote = (EditText) dialogSheetView.findViewById(R.id.editNote);
        Button btbChange = (Button) dialogSheetView.findViewById(R.id.btnChange);

        editId.setText(info.get(i).getId());
        editName.setText(info.get(i).getName());
        editNote.setText(info.get(i).getNote());

        btbChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String Id = editId.getText().toString().trim();
                final String Name = editName.getText().toString().trim();
                final String Note = editNote.getText().toString().trim();

                infor.setId(Id);
                infor.setId(Name);
                infor.setId(Note);
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSave:
                final String id = editId.getText().toString().trim();
                final String name = editName.getText().toString().trim();
                final String note = editNote.getText().toString().trim();

                information = new Info(id, name, note);
                info.add(information);
                lvInfo.setAdapter(infoAdapter);
                infoAdapter.notifyDataSetChanged();

                infoAdapter.notifyDataSetChanged();
                lvInfo.setAdapter(infoAdapter);

                break;
            default:
                break;

        }
    }
}