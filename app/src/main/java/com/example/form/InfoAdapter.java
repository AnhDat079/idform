package com.example.form;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class InfoAdapter extends BaseAdapter {

    private MainActivity context;
    private List<Info> info;
    private int layout;

    public InfoAdapter(MainActivity context, List<Info> info, int layout) {
        this.context = context;
        this.info = info;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return info.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout,null);

        TextView infoID = (TextView) view.findViewById(R.id.infoID);
        TextView infoNAME = (TextView) view.findViewById(R.id.infoNAME);
        TextView infoNOTE = (TextView) view.findViewById(R.id.infoNOTE);
        ImageButton btnEdit = (ImageButton) view.findViewById(R.id.btnEdit);
        ImageButton btnDelete = (ImageButton) view.findViewById(R.id.btnDelete);

        final Info infor = info.get(i);

        infoID.setText(infor.getId());
        infoNAME.setText(infor.getName());
        infoNOTE.setText(infor.getNote());

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.Edit(i);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.Delete(i);
            }
        });

        return view;
    }
}
